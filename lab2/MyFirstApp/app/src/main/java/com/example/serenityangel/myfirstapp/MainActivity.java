package com.example.serenityangel.myfirstapp;

import android.support.annotation.StringRes;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import static com.example.serenityangel.myfirstapp.R.id.activity_main;
import static com.example.serenityangel.myfirstapp.R.id.editText;
import static com.example.serenityangel.myfirstapp.R.id.showText;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button myButton = (Button) findViewById(R.id.button);
        myButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText txtDescription = (EditText) findViewById(R.id.editText);
                String inputText = txtDescription.getText().toString();
                TextView textView = (TextView) findViewById(showText);
                textView.setText(inputText);
            }
        });
    }
}

